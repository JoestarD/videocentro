<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user -> name = "Erick";
        $user -> lastname = "Diaz";
        $user -> email = "ericksbas@gmail.com";
        $user -> date_of_birth = "1998-06-12";
        $user -> phone_number = "6122434547";
        $user -> password =  bcrypt("12345678");
        $user -> role = 1;
        $user -> save();

        $user = new User;
        $user -> name = "Diego";
        $user -> lastname = "Diaz";
        $user -> email = "diego@gmail.com";
        $user -> date_of_birth = "1998-06-12";
        $user -> phone_number = "6122434347";
        $user -> password =  bcrypt("diego123");
        $user -> role = 2;
        $user -> save();

    }
}
